from django.contrib import admin

# Register your models here.
from chores.models import Person, Task, Assignment

admin.site.register(Person)
admin.site.register(Task)
admin.site.register(Assignment)
