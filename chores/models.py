import datetime

from django.db import models

# Create your models here.

class Person(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name

class Task(models.Model):
    name       = models.CharField(max_length=100)
    difficulty = models.IntegerField()

    def __str__(self):
        return self.name

class Assignment(models.Model):
    person        = models.ForeignKey(Person)
    task          = models.ForeignKey(Task)
    date_finished = models.DateField(blank=True, null=True)
    date_created  = models.DateField(auto_now_add=True)
    start_week    = models.DateField(verbose_name="starting week")
    done_before   = models.DateField(verbose_name="to be done before")

    def __str__(self):
        days_left = (self.done_before - datetime.date.today()).days
        return f"{self.person} --- {self.task} -- before {self.done_before} (days left: {days_left})"
