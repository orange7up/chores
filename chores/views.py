from typing import List

from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from chores.models import Assignment, Person


def home(request):
    assignments = Assignment.objects.order_by("-date_created")

    mis = Person.objects.get(name='Miś')
    olenka = Person.objects.get(name='Oleńka')

    mis_assignments = assignments.filter(person__name=mis.name)
    olenka_assignments = assignments.filter(person__name=olenka.name)

    context = {'assignments': {mis.name: mis_assignments, olenka.name: olenka_assignments}}
    return render(request, "index.html", context)